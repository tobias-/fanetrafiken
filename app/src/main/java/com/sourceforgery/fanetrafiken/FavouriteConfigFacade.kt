package com.sourceforgery.fanetrafiken

import com.sourceforgery.config.Station
import com.sourceforgery.config.Trip

interface FavouriteConfigFacade {
    suspend fun toggleFavouriteStation(station: Station)
    suspend fun toggleFavouriteTrip(trip: Trip)
    suspend fun addLastStation(station: Station)
    suspend fun addLastTrip(trip: Trip)
    suspend fun getFavouriteStations(): List<Station>
    suspend fun getFavouriteTrips(): List<Trip>
    suspend fun getLastStations(): List<Station>
    suspend fun getLastTrips(): List<Trip>
}
