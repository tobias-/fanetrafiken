package com.sourceforgery.fanetrafiken

import android.os.Bundle
import androidx.annotation.DrawableRes
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.Composable
import androidx.compose.unaryPlus
import androidx.ui.core.Alignment
import androidx.ui.core.Dp
import androidx.ui.core.Modifier
import androidx.ui.core.Text
import androidx.ui.core.TextUnit
import androidx.ui.core.TextUnit.Companion.Em
import androidx.ui.core.WithDensity
import androidx.ui.core.setContent
import androidx.ui.foundation.VerticalScroller
import androidx.ui.graphics.Color
import androidx.ui.graphics.ScaleFit
import androidx.ui.graphics.vector.DrawVector
import androidx.ui.layout.AspectRatio
import androidx.ui.layout.Column
import androidx.ui.layout.Container
import androidx.ui.layout.ExpandedHeight
import androidx.ui.layout.ExpandedWidth
import androidx.ui.layout.FlexRow
import androidx.ui.layout.Row
import androidx.ui.layout.Size
import androidx.ui.layout.Spacing
import androidx.ui.material.Divider
import androidx.ui.material.MaterialTheme
import androidx.ui.material.surface.Surface
import androidx.ui.res.vectorResource
import androidx.ui.text.TextStyle
import androidx.ui.tooling.preview.Preview

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SelectTrip()
        }
    }
}

@Composable
fun SelectTrip() {
    MaterialTheme {
        VerticalScroller {
            Column {
                Box(
                    icon = R.drawable.search,
                    label = "Sök resa",
                    desc = "Resealternativ mellan två hållplatser/stationer"
                )

                ThinDivider()

                Box(
                    icon = R.drawable.search,
                    label = "Sök station",
                    desc = "Avgångar/ankomster för en hållplats"
                )

                ThinDivider()

                Box(
                    icon = R.drawable.about,
                    label = "Om",
                    desc = "Om Fånetrafiken"
                )

                Headline {
                    Text("Favoritsökningar")
                }
                Headline {
                    Text("Tidigare sökningar")
                }
            }
        }
    }
}

@Composable
fun ThinDivider() {
    Divider(
        color = (+MaterialTheme.colors()).primary,
        height = Dp(2f)
    )
}

@Composable
fun VectorImage(
    modifier: Modifier = Modifier.None, @DrawableRes id: Int,
    tint: Color = Color.Transparent
) {
    val vector = +vectorResource(id)
    WithDensity {
        Container(
            modifier = modifier wraps Size(vector.defaultWidth.toDp(), vector.defaultHeight.toDp())
        ) {
            DrawVector(vector, tint)
        }
    }
}

@Composable
fun Box(
    @DrawableRes icon: Int,
    label: String,
    desc: String
) {
    Row {
        Container(
            width = Dp(80f),
            height = Dp(80f),
            alignment = Alignment.Center
        ) {
            DrawVector(
                vectorImage = +vectorResource(icon),
                alignment = Alignment.Center,
//                fit = ScaleFit.FillHeight,

                tintColor = (+MaterialTheme.colors()).primary
            )
        }
        Column {
            Text(
                text = label,
                maxLines = 1,
                style = TextStyle(fontSize = TextUnit.Companion.Em(8.0f))
            )
            Text(
                text = desc,
                maxLines = 1,
                style = TextStyle(fontSize = TextUnit.Companion.Em(3f))
            )
        }
    }

}

@Composable
fun Headline(children: @Composable() () -> Unit) {

    Surface(
        modifier = ExpandedWidth wraps Spacing(top = Dp(1f), bottom = Dp(1f)),
        color = (+MaterialTheme.colors()).primary,
        children = children
    )
}

@Preview
@Composable
fun DefaultPreview() {
    SelectTrip()
}
