package com.sourceforgery.fanetrafiken

import android.content.Context
import android.util.Log
import com.sourceforgery.config.ProtobufFavouriteConfig
import com.sourceforgery.config.Station
import com.sourceforgery.config.Trip
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.generic.instance

class ProtobufFavouriteConfigFacade(
    override val kodein: Kodein
) : FavouriteConfigFacade, KodeinAware {

    private val ctx: Context by instance()

    @Suppress("EXPERIMENTAL_API_USAGE")
    private val configRunner = newSingleThreadContext("ConfigRunner")

    private var data = GlobalScope.async(configRunner) {
        try {
            @Suppress("BlockingMethodInNonBlockingContext")
            ctx.openFileInput(filename).use {
                ProtobufFavouriteConfig.parseFrom(it)
            }
        } catch (e: Exception) {
            Log.i("GrpcConfigFacade", "Could not find/read $filename")
            ProtobufFavouriteConfig.getDefaultInstance()
        }
    }

    private fun writeProtobuf() {
        GlobalScope.launch(configRunner) {
            @Suppress("BlockingMethodInNonBlockingContext")
            ctx.openFileOutput(filename, Context.MODE_PRIVATE).use {
                it.write(data.await().toByteArray())
            }
        }
    }

    private suspend fun replaceData(block: (builder: ProtobufFavouriteConfig.Builder, data: ProtobufFavouriteConfig) -> Unit) {
        val data = this@ProtobufFavouriteConfigFacade.data.await()
        val builder = data.newBuilderForType()
        block(builder, data)
        this@ProtobufFavouriteConfigFacade.data = CompletableDeferred(builder.build())
        writeProtobuf()
    }


    override suspend fun toggleFavouriteStation(station: Station) {
        replaceData { builder, data ->
            builder.clearFavouriteStations()
            val oldStation = data.favouriteStationsList.firstOrNull { it.id == station.id }
            if (oldStation == null) {
                builder.addFavouriteStations(station)
                builder.addAllFavouriteStations(data.favouriteStationsList)
            } else {
                builder.addAllFavouriteStations(data.favouriteStationsList.filter { it.id == station.id })
            }
        }
    }

    override suspend fun toggleFavouriteTrip(trip: Trip) {
        replaceData { builder, data ->
            val oldTrip =
                data.favouriteTripsList.firstOrNull { it.from.id == trip.from.id && it.to.id == trip.to.id }
            builder.clearFavouriteTrips()
            if (oldTrip == null) {
                builder.addFavouriteTrips(trip)
                builder.addAllFavouriteTrips(data.favouriteTripsList)
            } else {
                builder.addAllFavouriteTrips(data.favouriteTripsList.filter { it.from.id == trip.from.id && it.to.id == trip.to.id })
            }
        }
    }

    override suspend fun addLastStation(station: Station) {
        replaceData { builder, data ->
            builder.clearLastStations()
                .addLastStations(station)
                .addAllLastStations(
                    data.lastStationsList
                        .filterNot { it.id == station.id }
                        .take(maxLast)
                )
        }
    }

    override suspend fun addLastTrip(trip: Trip) {
        replaceData { builder, data ->
            builder.clearLastTrips()
                .addLastTrips(trip)
                .addAllLastTrips(data.lastTripsList
                    .filterNot { it.from.id == trip.from.id && it.to.id == trip.to.id }
                    .take(maxLast)
                )
        }
    }

    override suspend fun getFavouriteStations(): List<Station> =
        data.await().favouriteStationsList

    override suspend fun getFavouriteTrips(): List<Trip> =
        data.await().favouriteTripsList

    override suspend fun getLastStations(): List<Station> =
        data.await().lastStationsList

    override suspend fun getLastTrips(): List<Trip> =
        data.await().lastTripsList

    companion object {
        private val filename = "favourites.protobuf"
        private const val maxLast = 9
    }
}
