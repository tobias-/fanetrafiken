package com.sourceforgery.fanetrafiken

import android.app.Application
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.singleton

class FanetrafikenApp : Application(), KodeinAware {
    override val kodein by Kodein.lazy {
        import(androidXModule(this@FanetrafikenApp))
        bind<FavouriteConfigFacade>() with singleton { ProtobufFavouriteConfigFacade(kodein) }
    }
}
