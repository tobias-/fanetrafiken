import com.google.protobuf.gradle.*
import org.gradle.api.JavaVersion.VERSION_1_8
import org.jetbrains.kotlin.config.KotlinCompilerVersion

plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    id("com.google.protobuf")
}

android {
    compileSdkVersion(29)
    buildToolsVersion("29.0.2")
    defaultConfig {
        applicationId = "com.sourceforgery.fanetrafiken"
        minSdkVersion(21)
        targetSdkVersion(29)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    compileOptions {
        sourceCompatibility = VERSION_1_8
        targetCompatibility = VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        compose = true
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:3.11.0"
    }
    plugins {
        id("javalite") { artifact = "com.google.protobuf:protoc-gen-javalite:3.0.0" }
    }
    generateProtoTasks {
        all().configureEach {
            plugins {
                id("javalite") { }
            }
        }
    }
}

dependencies {
    //    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(kotlin("stdlib-jdk7", KotlinCompilerVersion.VERSION))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.3")

//    implementation("com.android.support.constraint:constraint-layout:1.1.3")

    val androidxVersion = "1.1.0"
    val jetpackComposeVersion = "0.1.0-dev03"
    implementation("androidx.core:core-ktx:$androidxVersion")
    implementation("androidx.appcompat:appcompat:$androidxVersion")
    implementation("androidx.ui:ui-foundation:$jetpackComposeVersion")
    implementation("androidx.ui:ui-framework:$jetpackComposeVersion")
    implementation("androidx.ui:ui-layout:$jetpackComposeVersion")
    implementation("androidx.ui:ui-material:$jetpackComposeVersion")
    implementation("androidx.ui:ui-text:$jetpackComposeVersion")
    compileOnly("androidx.ui:ui-tooling:$jetpackComposeVersion")

    val kodeinVersion: String by project
    implementation("org.kodein.di:kodein-di-generic-jvm:$kodeinVersion")
    implementation("org.kodein.di:kodein-di-framework-android-x:$kodeinVersion")

//    implementation("com.moandjiezana.toml:toml4j:0.7.1")
    implementation("com.google.protobuf:protobuf-lite:3.0.1")

    testImplementation("junit:junit:4.12")
    androidTestImplementation("androidx.test:runner:1.2.0")
    androidTestImplementation("androidx.test.ext:junit:1.1.1")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.2.0")
}
