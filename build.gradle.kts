buildscript {
    repositories {
        google()
        jcenter()

    }

    dependencies {
        val kotlinVersion: String by project
        classpath("com.android.tools.build:gradle:4.0.0-alpha08")
        classpath("com.google.protobuf:protobuf-gradle-plugin:0.8.11")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
    }
}

allprojects {
    repositories {
        google()
        jcenter()
    }
}

tasks.register<Delete>("clean") {
    delete(rootProject.buildDir)
}
